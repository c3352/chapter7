const router = require("express").Router()
const controller = require("../controllers")
const restrict = require("../middleware/restrict")
const restrictJwt = require("../middleware/restrictJwt")

router.get('/whoami', restrictJwt, controller.whoamiApi)
router.get('/', controller.landingPage)
router.get('/game', restrict, controller.game)
router.get('/login', controller.loginGet)
router.post('/login', controller.loginPost)
router.post('/loginjwt', controller.loginJwt)
router.get('/dashboard', restrict, controller.dashboard)
router.get('/create', controller.createGet)
router.post('/create', controller.signup)
router.get('/update/:id', restrict, controller.updateGet)
router.post("/update/:id", restrict, controller.updatePost)
router.get("/delete/:id", restrict, controller.deleteUser)
router.post('/generate', controller.generate)
router.post('/join', controller.join)
router.post('/fight', controller.fight)
router.get('/result', controller.result)


module.exports = router
