const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const {user_game} = require('../models')


async function authenticate(username, password, done){
    try {
        const User_game = await user_game.authenticate({username, password})

        return done(null, User_game)
    } 
    catch (error) {
        return done(null, false, {message: error.message})
    }
}

passport.use(
    new LocalStrategy({usernameField: 'username', passwordField: 'password'}, authenticate)
)

passport.serializeUser(
    (User_game, done) => done(null, User_game.id)
)

passport.deserializeUser(
    async (id, done) => done(null, await user_game.findByPk(id))
)

module.exports = passport