//pilihan random computer
function getPilihanComputer() {
  const comp = Math.random();
  if (comp < 0.34) return "gunting";
  if (comp >= 0.34 && comp < 0.67) return "batu";
  return "kertas";
}

//hasil suit
function getHasil(comp, player) {
  if (player == comp) return "DRAW";
  if (player == "gunting") return comp == "kertas" ? "PLAYER 1 WIN" : "COM WIN";
  if (player == "batu") return comp == "kertas" ? "COM WIN" : "PLAYER 1 WIN";
  if (player == "kertas") return comp == "gunting" ? "COM WIN" : "PLAYER 1 WIN";
}

//PILIHAN BATU PLAYER
const pBatu = document.querySelector(".batu");
pBatu.addEventListener("click", function () {
  const pilihanComputer = getPilihanComputer();
  const pilihanPlayer = pBatu.className;
  const hasil = getHasil(pilihanComputer, pilihanPlayer);
  // putar();

  // setTimeout(function () {
  const imgComputerBatu = document.querySelector(".batu1");
  if (pilihanPlayer == pilihanComputer) {
    imgComputerBatu.style.backgroundColor = "#cacaca";
    imgComputerBatu.style.padding = "5px";
    imgComputerBatu.style.margin = "5px";
    imgComputerBatu.style.borderRadius = "20px";
  }

  const imgComputerKertas = document.querySelector(".kertas1");
  if (pilihanPlayer == "batu" && pilihanComputer == "kertas") {
    imgComputerKertas.style.backgroundColor = "#cacaca";
    imgComputerKertas.style.padding = "5px";
    imgComputerKertas.style.margin = "5px";
    imgComputerKertas.style.borderRadius = "20px";
  }

  const imgComputerGunting = document.querySelector(".gunting1");
  if (pilihanPlayer == "batu" && pilihanComputer == "gunting") {
    imgComputerGunting.style.backgroundColor = "#cacaca";
    imgComputerGunting.style.padding = "5px";
    imgComputerGunting.style.margin = "5px";
    imgComputerGunting.style.borderRadius = "20px";
  }

  const info = document.querySelector(".ket");
  info.innerHTML = hasil;
  // }, 1000);
});

//PILIHAN GUNTING PLAYER
const pGunting = document.querySelector(".gunting");
pGunting.addEventListener("click", function () {
  const pilihanComputer = getPilihanComputer();
  const pilihanPlayer = pGunting.className;
  const hasil = getHasil(pilihanComputer, pilihanPlayer);

  const imgComputerBatu = document.querySelector(".gunting1");
  if (pilihanPlayer == pilihanComputer) {
    imgComputerBatu.style.backgroundColor = "#cacaca";
    imgComputerBatu.style.padding = "5px";
    imgComputerBatu.style.margin = "5px";
    imgComputerBatu.style.borderRadius = "20px";
  }

  const imgComputerKertas = document.querySelector(".kertas1");
  if (pilihanPlayer == "gunting" && pilihanComputer == "kertas") {
    imgComputerKertas.style.backgroundColor = "#cacaca";
    imgComputerKertas.style.padding = "5px";
    imgComputerKertas.style.margin = "5px";
    imgComputerKertas.style.borderRadius = "20px";
  }

  const imgComputerGunting = document.querySelector(".batu1");
  if (pilihanPlayer == "gunting" && pilihanComputer == "batu") {
    imgComputerGunting.style.backgroundColor = "#cacaca";
    imgComputerGunting.style.padding = "5px";
    imgComputerGunting.style.margin = "5px";
    imgComputerGunting.style.borderRadius = "20px";
  }

  const info = document.querySelector(".ket");
  info.innerHTML = hasil;
});

//PILIHAN KERTAS PLAYER
const pKertas = document.querySelector(".kertas");
pKertas.addEventListener("click", function () {
  const pilihanComputer = getPilihanComputer();
  const pilihanPlayer = pKertas.className;
  const hasil = getHasil(pilihanComputer, pilihanPlayer);

  const imgComputerBatu = document.querySelector(".kertas1");
  if (pilihanPlayer == pilihanComputer) {
    imgComputerBatu.style.backgroundColor = "#cacaca";
    imgComputerBatu.style.padding = "5px";
    imgComputerBatu.style.margin = "5px";
    imgComputerBatu.style.borderRadius = "20px";
  }

  const imgComputerKertas = document.querySelector(".gunting1");
  if (pilihanPlayer == "kertas" && pilihanComputer == "gunting") {
    imgComputerKertas.style.backgroundColor = "#cacaca";
    imgComputerKertas.style.padding = "5px";
    imgComputerKertas.style.margin = "5px";
    imgComputerKertas.style.borderRadius = "20px";
  }

  const imgComputerGunting = document.querySelector(".batu1");
  if (pilihanPlayer == "kertas" && pilihanComputer == "batu") {
    imgComputerGunting.style.backgroundColor = "#cacaca";
    imgComputerGunting.style.padding = "5px";
    imgComputerGunting.style.margin = "5px";
    imgComputerGunting.style.borderRadius = "20px";
  }

  const info = document.querySelector(".ket");
  info.innerHTML = hasil;
});
