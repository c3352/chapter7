const express = require("express")
const session = require('express-session')
const flash = require('express-flash')
const app = express();
const port = 3000;
const router = require ("./router")
const logger = require("./middleware/logger.js")

app.use(session({
    secret: 'buat ini jadi rahasia',
    resave: false,
    saveUninitialized: false
}))
const passport = require('./lib/passport')
app.use(passport.initialize())
app.use(passport.session())
app.use(flash())

app.set('view engine', 'ejs')
app.use(express.static("./public"));
app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use(logger)
app.use(router)

app.use((err, req, res, next) => {
    console.log(err);
    res.status(500).json({
        status: 'fail',
        errors: err.message
    })
    next()
})

app.use((req, res, next) => {
    res.status(404).json({
        status: 'fail',
        errors: 'Page not found'
    })
    next()
})


app.listen(port, () => console.log(`Server listening on port: ${port}`))