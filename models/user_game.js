'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user_game.hasOne(models.user_game_biodata, {
        onDelete: "cascade"
      });
      user_game.hasMany(models.user_game_history, {
        onDelete: "cascade"
      });
    }

    static #encrypt = (password) => bcrypt.hashSync(password, 10)

    static updateUser = ({email, username, password}) => {
      const encryptedPassword = this.#encrypt(password)
      return {email, username, password: encryptedPassword}
    }


    static register = ({username, password}) => {
      const encryptedPassword = this.#encrypt(password)

      return this.create({username, password: encryptedPassword})
    }

    static authenticate = async ({username, password}) => {
      try {
        const user = await this.findOne({where: {username}})
        if(!user) return Promise.reject("User not found!")
        const isPasswordValid = user.checkPassword(password)
        if(!isPasswordValid) return Promise.reject("Wrong password!")
        return Promise.resolve(user)
      } catch (error) {
        return Promise.reject(error)
      }

    }

    checkPassword = password => bcrypt.compareSync(password, this.password)

    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.username
      }

      const rahasia = 'Ini rahasia ga boleh di sebar sebar'

      const token = jwt.sign(payload, rahasia)

      return token
    }

    
  };
  user_game.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_game',
  });
  return user_game;
};